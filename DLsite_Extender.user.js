// ==UserScript==
// @name         DLsite_Extender
// @namespace    uraunagi
// @version      0.1
// @description  とりあえずDLsiteのレビューでタグ付けを支援する
// @author       uraunagi
// @match        https://www.dlsite.com/*/review/write/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    if(window == window.parent){
        console.log("DLsite_Extender START");

        //タグ設定用のチェックボックスを全て列挙する
        var checkbox_list = document.querySelectorAll("div.frame_double_list_box > div > dl > dd > input");

        //最終確認画面だとチェックボックスがないので、その場合はスクリプトを作動させない
        if(checkbox_list.length > 0){
            //NodeList型のままだと扱いづらいので配列化しておく
            checkbox_list = Array.prototype.slice.call(checkbox_list);

            //投稿ボタンの下あたりにタグ数表示用のラベルを作る
            var ext_label = document.createElement('p');
            ext_label.className = "btn_explain center";
            document.querySelector(".btn_box").appendChild(ext_label);

            //タグ数オーバーの時に色を変える領域
            var warning_element = document.querySelector("td.frame_double_list");

            //チェックボックスの状態が変更されると呼ばれる関数
            var chkedit_event = (()=>{
                var tag_count = checkbox_list.reduce((result, current) => result + Number(current.checked), 0);
                ext_label.innerHTML = "現在のジャンル数("+tag_count+"/10)<br />"+(document.getElementById("comic").checked ? "<font color='red'>作品をおすすめする</font>" : "作品をおすすめしない");
                if(10<tag_count){
                    //背景を赤くする
                    warning_element.className = "frame_double_list input_error";
                }else{
                    //背景の色を正常にもどす
                    warning_element.className = "frame_double_list";
                }
            });

            //全チェックボックスにイベント設定、監視を始める
            for(const element of checkbox_list){
                element.addEventListener("change", chkedit_event);
            }

            //チェックボックスを設定
            var is_new_review = location.href!==document.referrer;
            if(location.href!==document.referrer){
                //レビューの書き始め
                //作品をおすすめするボタンにチェック
                document.getElementById("comic").checked = true;
            }else{
                //ジャンル数オーバー、もしくは最終確認で編集ボタンを押した場合
                //元の状態を維持するのでチェックボックスは更新しない
                console.log("[再編集画面]");
            }
            //おすすめにするのチェックボタンも、変更があればラベルを更新するためイベントが必要
            document.getElementById("comic").addEventListener("change", chkedit_event);

            //元の作品についてるタグは字の色を変える
            var default_tags = document.querySelectorAll(".search_tag > a");
            for(const element of default_tags){
                var tag_id = /\d\d\d/.exec(element.getAttribute("href"))[0];
                var checkbox = document.getElementById("genre_list["+tag_id+"]");
                checkbox.parentNode.style.backgroundColor = "#CCCCFF";
                //初期状態でチェックを入れておく
                if(is_new_review){
                    checkbox.checked = is_new_review;
                }
            }

            //最初の一回は手動でカウントを初期化しておく
            chkedit_event();
        }
    }
})();