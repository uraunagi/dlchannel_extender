// ==UserScript==
// @name         DLsite_TagGetter
// @namespace    uraunagi
// @version      0.2
// @description  DLsiteの詳細検索画面からタグを取得して、クリップボードに転送する（開発・分析用）
// @author       unaunagi
// @match        https://www.dlsite.com/*/fs*
// @grant        none
// ==/UserScript==

//検索画面はJQueryで動いているので、それに合わせて使う
(function() {
    'use strict';

    //メインの呼び出し以外は無視する
    if(window != window.parent){return;}

    $('span.switchHat_l').trigger('click');
    document.body.insertAdjacentHTML("afterbegin",'<div><input type="button" id="es_taggetter" style="width: 400px; height: 100px" value="DLchannel_Extender用\nタグ一覧をクリップボードに読み込み" /></div>');

    //クリップボードを使うために、いったんボタンを押させる
    let btn = document.getElementById('es_taggetter');

    btn.addEventListener('click', (e)=> {
        var result = "INDEX\tID\tNAME\tCOUNT\n"
        $("input[name|='genre[]']").each((idx,e)=>{
            const t = $(e).next("label").text().split(" ");
            const cnt = t[1].match(/\((.*)\)/);
            result += `${idx+1}\t${$(e).val()}\t${t[0]}\t${cnt[1]}\n`;
            //result += (idx+1). + "\n" + $(e).val() + "\t" + t[0] + "\t" + cnt[1] + "\n";
        });

        console.log(result);

        document.body.insertAdjacentHTML("beforeend",'<textarea id="extender_copy" style="position:fixed;left:-100%;">'+result+'</textarea>');
        document.getElementById('extender_copy').select();
        document.execCommand('copy');
        document.getElementById('extender_copy').remove();
    });

})();