## License

These codes are licensed under CC0.

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/deed.ja)


## 説明書

DLsiteやDLチャンネルで使うGreaseMonkeyスクリプトです。

詳しい使い方や更新履歴などはCi-enの記事にしているので、そちらを見てください。

### [DLチャンネルの記事編集を楽にするGreaseMonkeyスクリプト](https://ci-en.dlsite.com/creator/997/article/36314)
DLチャンネルの投稿者用のスクリプトです。
クリエイターズ記事を書いてる時に、間違って反映ボタンを押さないためのものです。
タグの色分け表示など便利な機能がたくさんあります。

### [DLsiteのレビューをする時に、作品についてるの同じタグにチェックを入れるやつ](https://ci-en.dlsite.com/creator/997/article/46844)
DLsiteでレビューを書く人向けのスクリプトです。
タグをつけるのが面倒だけど、つけないのもなんかなあということで作ったものです。
10個以上になると色が変わるので、タグのつけすぎを未然に防げます。

### [DLsiteのお気に入りリストの合計金額を調べるやつ](https://ci-en.dlsite.com/creator/997/article/177708)
DLsiteのヘビーユーザー向けスクリプトです。
お気に入りリスト内の合計金額を調べることができます。
