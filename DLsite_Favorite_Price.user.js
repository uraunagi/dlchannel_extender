// ==UserScript==
// @name         DLsite_Favorite_Price
// @namespace    uraunagi
// @version      0.1
// @description  DLsiteで表示中のお気に入りリストを調べて、今全部買うといくらになるか計算する
// @author       uraunagi
// @match        https://www.dlsite.com/*/mypage/wishlist*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    if(window == window.parent){
        console.log("DLsite_Favorite_Price START");

        var page_total = document.querySelector(".page_total");
        var view_area = document.createElement('td');
        var price_text = document.createElement('span');
        view_area.appendChild(price_text);
        page_total.parentNode.insertBefore(view_area, page_total.nextSibling);

        var priceList = document.querySelectorAll(".work_price");
        var total = 0;
        for (var i = 0; i < priceList.length; i++) {
            const p = priceList[i].innerHTML.replace(/\D/g,"")
            total += parseInt(p)
        }
        price_text.innerHTML = "全部買うと "+total+" 円です"

    }
})();