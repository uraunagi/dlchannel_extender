// ==UserScript==
// @name         DLChannel_Extender
// @namespace    uraunagi
// @version      1.0.3
// @description  DLチャンネルの記事編集用。タグを見分けやすくし、必要のない時に公開ボタンを押せなくします
// @author       uraunagi
// @match        https://*.dlsite.com/matome/*
// @match        https://*.dlsite.com/mypage*
// @match        https://*.dlsite.com/talk/*
// @match        https://ch.dlsite.com/lp/creators*
// @match        https://ch.dlsite.com/search/contents?content=matomes&tag=*
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

(function () {
    //※ユーザー用のタグは、まとめ画面の設定ボタンから設定できるようになりました
    //それはそれとしてスクリプト側へのタグ追加希望も受け付けています

    'use strict';

    //これらのタグを入れるとクリエイターズ記事予定だと判断して、公開ボタンを押せなくする
    const creator_tags = ["クリエイターズ記事","クリエイターズねくすと"];

    //下書き専用モードに移行させるためのタグ
    const draft_tags = ["下書き"];

    //クリエイターズねくすとや特別企画などのテーマタグ
    //自動取得するようにしてみる
    const campaign_tags = [];

    //キャンペーンが終了したタグ
    //通常タグにまぜることにした
    const campaign_old_tags = [];
    //const campaign_old_tags = ["プレイ動画","教えて！君の性癖","〇〇解説","もっと評価されるべき","インターネット","海外のはなし","マイナー好み描写選手権","ギリギリ健全","シリーズ作品","好きなサークル・作家・メーカーについて","好きなキャラクター","出会いのはなし","私の推し","昭和","和風","行ってみた・やってみた","クイズ・検定","小説・SS","体験談"];

    //DLsiteのジャンル検索で使うタグを全部入れてある（全年齢と女性向けのタグも入ってるはず）
    //最新版は通信読み込みだけど、ダウンロードするまで何もなしだとよくわからない人も出そうなので
    //初期状態でもあるていどまともに動くようにはしておく
    let fetish_tags2 = ["ASMR","OL","SF","SM","おさわり","おっぱい","おねショタ","おむつ","おもちゃ","おもらし","おやじ","お姉さん","お嬢様","お尻/ヒップ","きせかえ","くすぐり","くノ一","けもの/獣化","ごっくん/食ザー","ささやき","ぶっかけ","ぷに","ほのぼの","ぼて腹/妊婦","やおい","やんちゃ受け","アナル","アニメ","アヘ顔","アラブ","アンソロジー","イラマチオ","インテリ","ウェイトレス","エプロン","エルフ/妖精","オカルト","オナニー","オフィス/職場","オメガバース","オヤジ受け","オールハッピー","ガチムチ","ガテン系","ガーター","キャットファイト","ギャグ","ギャル","クンニ","クール受け","クール攻め","ゲイ/男同士","コスプレ","コメディ","ゴスロリ","サキュバス/淫魔","サスペンス","サラリーマン","シスター","ショタ","ショートカット","シリアス","シリーズもの","スカトロ","スクール水着","ストッキング","スパッツ","スパンキング","スポーツ","スレンダー","スーツ","セーラー服","ソフトエッチ","ゾンビ","チラリズム","ツインテール","ツルペタ","ツンデレ","ドット","ナース","ニプルファック","ニューハーフ","ニーソックス","ネコミミ","ノンケ","ノンフィクション/体験談","ノーマルプレイ","ハーレム","バイ","バイオレンス","バイノーラル/ダミヘ","バニーガール","パイズリ","パイパン","パンツ","ビッチ","ピアス/装飾品","ファンタジー","フィストファック","フェチ","フェラチオ","フタナリ","ブルマ","ヘタレ攻め","ホスト","ホラー","ボクっ娘","ボンデージ","ポニーテール","マニアック/変態","ミステリー","ミニスカ","ミリタリー","ムチ/縄/蝋燭","ムチムチ","メイド","メガネ","モブ姦","ヤクザ/裏社会","ヤリチン/プレイボーイ","ヤンデレ","ラバー","ラブコメ","ラブラブ/あまあま","リバ","リョナ","レイプ","レオタード","レスラー/格闘家","レズ/女同士","ロボット/アンドロイド","ロマンス","ロリ","ロングヘア","ローション","上司","下克上","下着","不良/ヤンキー","中出し","丸呑み","主従","乳首/乳輪","人体改造","人外娘/モンスター娘","人妻","体操着","体育会系/スポーツ選手","保健医","俺様","俺様攻め","健全","健気受け","催眠","催眠音声","兄","先輩/後輩","処女","出産","初体験","制服","動物/ペット","包茎","医者","双子","叔父/義父","口内射精","同居","同棲","同級生/同僚","和姦","執事","売春/援交","変身ヒロイン","外国人","天使/悪魔","天然","太め/デブ","女主人公","女体化","女医","女性コミック","女性視点","女教師","女王様/お姫様","女装","奴隷","妊娠/孕ませ","妖怪","妹","姉妹","娘","学校/学園","学生","実姉","寝取られ","寝取り","少女","少女コミック","少年","少年コミック","尿道","屋外","巨乳/爆乳","巨大化","巨根","巫女","年上","年下攻め","幼なじみ","幽霊","弟","強制/無理矢理","強気受け","強気攻","性転換(TS)","恋人同士","悪堕ち","感動","憑依","戦場","戦士","手コキ","拘束","拡張","拷問","搾乳","擬人化","放尿/おしっこ","放置プレイ","教師","料理/グルメ","断面図","方言","既婚者","日常/生活","時間停止","未亡人","格闘","機械姦","歳の差","歴史/時代物","母乳","母親","水着","汁/液大量","洗脳","浣腸","浮気","淡白/あっさり","淫乱","淫語","潮吹き","無表情","焦らし","熟女","燃え","父","狂気","猟奇","獣姦","獣耳","王子様/王子系","産卵","男の娘","男主人公","男子妊娠/出産","男性受け","異種姦","痴漢","癒し","百合","盗撮","監禁","着物/和服","着衣","睡眠姦","短髪","石化","社長","童貞","筋肉","純愛","緊縛","総受","総攻","羞恥/恥辱","義妹","義姉","義母","耳かき","耳舐め","耽美","脚","腹パン","色仕掛け","芸能人/アイドル/モデル","萌え","薬物","蟲姦","血液/流血","複乳/怪乳/超乳","複数プレイ/乱交","褐色/日焼け","襲い受","触手","言葉責め","評論","誘い受け","調教","警察/刑事","貧乳/微乳","足コキ","軍服","輪姦","近親相姦","退廃/背徳/インモラル","逆ハーレム","逆レイプ","逆転無し","連続絶頂","道具/異物","金髪","長身","陰毛/腋毛","陵辱","電車","露出","青姦","青年","青年コミック","靴下","顔射","風俗/ソープ","首輪/鎖/拘束具","鬱","鬼畜","魔法","魔法使い/魔女","魔法少女","黒髪"];

    //スクリプト側でデフォルトで用意しているタグ(とりあえずユーザー用タグと同じ色に)
    //最新版は通信読み込み
    let default_tags2 = [];
    //エイシス関係
    default_tags2 = default_tags2.concat(["DLsite","DLチャンネル","アテナちゃん","がるまに","にじGAME","にじよめちゃん","Ci_en","トリオキニ","でぃーえるねこ","エイシス"]);
    //作品形式
    //default_tags2 = default_tags2.concat(["Youtuber","Android","スマホ","ボードゲーム","カードゲーム","テーブルゲーム","ドラマCD","乙女向け","乙女向け音声作品","女性向け","女性向けR18同人音声作品","チップチューン","ゲーム音楽","シューティング","eスポーツ","VR","ソシャゲ","格闘ゲーム","オープンワールド","ADV","ノベルゲーム","フリーゲーム","特撮","TVアニメ","エロアニメ","ドットエロ","ツール","音楽","効果音","素材","画像素材","音素材","小説","ネット小説","官能小説","ライトノベル","映画","麻雀","パズル","同人ゲーム","同人誌","音声作品","同人音声","RPG","アクションRPG","エロRPG","TRPG","SLG","調教SLG","アクション","シューティング","音ゲー","レトロゲーム","3DCG","おま国"]);
    //その他ジャンル的タグ
    //default_tags2 = default_tags2.concat(["ポーズ集","Nintendo Switch","PC-9801","設定資料集","総集編","リメイク","プロレス","夏休み","クロスオーバー","野球","サッカー","競馬","パチンコ","ギャンブル","タワーディフェンス","処女プレイ可","ゲームシステム","異世界","スーパーファミコン","メガドライブ","ファミコン","PlayStation","セガサターン","エロステータス","ゲーム機","家庭用ゲーム","全年齢作品","バイノーラル","耳かき","バトルファック","グロ","ドMホイホイ","戦闘中エロ","二次創作","擬人化","Vtuber","うんこ無修正","海外エロゲー","体験談","仲間とセックスできるゲーム","主人公としかセックスしないゲーム","ふたなり主人公","男無"]);
    //キャラクター設定・容姿
    //default_tags2 = default_tags2.concat(["悪女","モブキャラ","種付けおじさん","ダークエルフ","エロ衣装","勇者","魔王","女騎士","メスガキ","ロリビッチ","スポユニ","チャイナ","褐色肌","日焼け跡","男装","和彫り","タトゥー/刺青","ふんどし","肚兜","筋肉娘","ケツ毛","シーメール","メス男子","ショタおね","乳首チンコ","黒乳首","グロマン","爆根","四肢切断"]);
    //状況・フェチ
    //default_tags2 = default_tags2.concat(["レズレイプ","脱衣","後ろ姿","人格否定","太眉","肋骨","逆リョナ","正式にパンツを脱ぐ作品","親子丼","母子相姦","男×ふた","ふた×男","ふた×ふた","メス堕ち","キメセク","アナル舐め","モブ姦","ギザ歯","段階エロ","足フェチ","ダーツ","タバコ","オナラ","ドライ","敗北エロ","女性上位","仰け反り絶頂","陰姦","蟹股ポージング","全裸","無知シチュ","ゲロ","ボコォ","男オナニー","無様エロ","変態洗脳","HEIZEN","フェベチオ","チンカス","臭いフェチ","身体に落書き","玉責め","子宮脱","食糞","卵巣","蟲姦","エロ処刑"]);
    //作品・シリーズ名など
    //default_tags2 = default_tags2.concat(["キズナアイ","天原帝国","異種族レビュアーズ","ラブライブ！","ダンジョンに出会いを求めるのは間違っているだろうか","Mount&amp;Blade2:Bannerlord","Mount&amp;Blade","SRPG Studio","テトリス","きらら","なかよし","コミックボンボン","ジャンプ","ガンダム","さばげぶっ!","賭博黙示録カイジ","ミスター味っ子","ゆるゆり","プリキュア","らんま1/2","うる星やつら","賭博黙示録カイジ","聖闘士星矢","キン肉マン","ゲーム・オブ・スローンズ","戦艦少女","ストリートファイター","クイーンズブレイド","ARK","けものフレンズ","女神転生","ペルソナ","アイドルマスター","アイドルマスターシンデレラガールズ","ログ・ホライズン","機動戦艦ナデシコ","RPGツクール","スーパーロボット大戦","ソード・ワールド","プリパラ","ワンピース","NARUTO","名探偵コナン","進撃の巨人","対魔忍","このすば","わたモテ","VOCALOID","VOICEROID","Fate","Fate/GrandOrder","ダイの大冒険","ポケットモンスター","咲-Saki-","スーパーマリオブラザーズ","グランブルーファンタジー","セーラームーン","ファイアーエムブレム","ゼルダの伝説","ジョジョの奇妙な冒険","ファイナルファンタジー","ドラゴンクエスト","艦これ","アズールレーン","ドールズフロントライン","ガールズ&amp;パンツァー","東方project","東方二次創作ゲーム"]);
    //各種企業・イベントなど
    //default_tags2 = default_tags2.concat(["BOOTH","Netflix","ソフコン","Aコン","にじさんじ","同人誌即売会","pixiv","pixiv小説","小説家になろう","ノクターンノベルズ","新都社","2ちゃんねる","ニコニコ動画","youtube","アリスソフト","LIBIDO","セガ","スクウェア・エニックス","KADOKAWA","コーエーテクモゲームス","プラチナゲームズ","ファルコム","エンジェル出版","ワニマガジン","茜新社","キルタイムコミュニケーション","ノベルゲームコレクション","EVO","コミケ","ふたけっと","りょなけっと","コミティア","ワンフェス","Steam","Amazon","DMM","FANZA"]);
    //それ以外のタグ
    //default_tags2 = default_tags2.concat(["熟成下書き供養祭り","チラシの裏","まとめのまとめ","初心者向け","聖地巡礼","動画作成","ゲーム制作","ジャンル特集","DLチャンネルをなんだと思ってるの？","購入者レビュー","体験版","5000字以上の記事","10000字以上の記事","ライフハック","トークから生まれたまとめ","ゲームシステム","性癖語り","英語"]);

    //ユーザー拡張用2 正規表現リテラルで表記
    //コミケ用タグ Cに続いて数字を続ける C**、もしくはC1**の場合だけ色がつくように)
    //今季アニメ用タグ 2000～2099年まで対応
    const default_regex_tags = [/^C1?\d{2}$/,/^20\d{2}年[冬春夏秋]アニメ$/];

    //これと部分一致するものは除外。通信読み込み対応
    var ng_tags = ["","／","　","＃","クリエイター","クリエーター","Ｒ１８","Ｒ18","ＤＱ","３DCG"];

    //入力補完時に特殊判定するタグ。通信読み込み対応
    var suggest_tags = {}

    //-------------------------------------------------------------------------------------------------------------

    //重要なエレメント
    let item_div,tag_div,autocomp_div,side_div;
    //独自の入力補完用
    let tag_input,tagspan_div;

    //ユーザー拡張用。設定画面から書き換えるように変更
    let user_tags = [];
    //キャンペーンタグはユーザー設定のを使う場合もあるので
    //実際に使う方をcampaign_tags2に設定する
    let campaign_tags2 = campaign_tags;
    // サーバーからの読み込みに備えて、初期登録用のと変数名をわける
    let default_tags = default_tags2;
    let fetish_tags = fetish_tags2;

    //一致検索用に正規化したタグ一覧
    //2の方は動的に変わるので、その都度作り直す必要あり
    let all_tags,all_tags2;
    let all_tags_chk,all_tags2_chk;

    //自前サジェストで入力中、終わるまで待つ
    let is_tag_complite = false;

    //タグ文字列の正規化（自前補完用）
    const normalize_tag = (tag)=>{
        let t = tag.toLowerCase();
        //補完の邪魔になりそうな文字は除外して探す
        //長音記号を除外するのは略称の判定を簡単にするため
        t = t.replace(/[\s\/／-～#＃「」『』\[\]\(\)（）・\!！\?？☆★'"ー]/g, '');
        t = t.normalize("NFKC");
        t = t.replace(/[ぁ-ん]/g, function(s) {
            return String.fromCharCode(s.charCodeAt(0) + 0x60);
        });
        t = t.replace(/&amp;/g, '&');
        return t;
    }
    const normalize_tags = (tags)=>{
        return tags.map((t)=>{return normalize_tag(t);});
    }

    //特殊入力補完用
    let suggest_tags2 = {};

    //タグの名前と使用数
    let hashTagCounter = {};

    //フラグ
    let is_creator_shinseichu = false;
    let is_matome = true;//まとめ記事の場合はtrue
    let is_edit = false;//編集中の場合はtrue
    let is_ng_chk = false;//データから読み込む

    //文字列の短い方を長い方に部分一致させた結果を返す関数
    const indexOf2=(a,b)=>{
        if(a.length > b.length){
            return a.indexOf(b);
        }else{
            return b.indexOf(a);
        }
    };

    //現在時を基準にタイムスタンプを作成
    const get_timestamp=()=>{
        const date = new Date();
        const year = date.getFullYear();
        const month= date.getMonth() + 1;
        const day = date.getDate();
        return year + '/' + month + '/' + day;
    }

    //そのタグがNGかどうかを判別
    const isNG=(tag)=>{
        let flag = true;
        ng_tags.forEach((v) => {
            if(tag.indexOf(v)>-1){
                flag = false;
            }
        });
        //スペースと/による区切り
        let sp = tag.split(/[ \/]/);
        if(sp.length >= 5){
            flag = false;
        } else if(sp.length > 1){
            fetish_tags.forEach((v) => {
                if(v.indexOf(sp[0])>-1){
                    flag = false;
                }
            });
        }
        return flag;
    };

    //タグの色を設定する関数。色合いの変更もここで。
    //入力補完時は文字色は反映されない（その方が選択中のやつを見分けやすいから）
    const getTagColor = function(tag){
        const result = {};
        result.background = ''; //背景色。一致がなければ元の色に戻す
        result.color = ''; //文字色。タグの色が明るい時などに見やすいように
        result.creator = false; //クリエイターズタグかどうか
        result.draft = false; //下書きタグかどうか
        result.visible = true; //表示するタグかどうか

        //上にあるほど色の優先順位が高くなる
        if((draft_tags.indexOf(tag)) >= 0){
            //下書き系タグと一致した場合
            result.draft = true;
            result.background = "#888888";
        }
        else if((creator_tags.indexOf(tag)) >= 0){
            //クリエイターズ系タグと一致した場合
            result.creator = true;
            result.background = "#FFFF44";
            result.color = "Black";
        }
        else if((campaign_tags2.indexOf(tag)) >= 0){
            //企画系タグと一致した場合
            result.background = "#AAFF00";
            result.color = "Black";
        }
        else if((user_tags.indexOf(tag)) >= 0){
            //ユーザー作成タグと一致した場合
            result.background = "#FF88FF";
        }
        else if((default_tags.indexOf(tag)) >= 0){
            //標準タグと一致した場合
            result.background = "#FF88FF";
        }
        else if(regex_check(tag,default_regex_tags)){
            //正規表現による標準タグと一致した場合
            result.background = "#FF88FF";
        }else if((fetish_tags.indexOf(tag)) >= 0){
            //DLsiteの趣味嗜好タグと一致した場合
            result.background = "#FF4488";
        }
        else if((campaign_old_tags.indexOf(tag)) >= 0){
            //企画が終了しているタグと一致した場合
            result.background = "#88A1C6";
        }else{
            //NGタグの設定
            result.visible = isNG(tag);

            //NG時用色設定
            if(!result.visible){
                result.background = "Black";
                result.color = "White";
            }
        }
        return result;
    }

    //タグと正規表現リテラルの配列が合致するか調べる
    const regex_check = function(tag,regex_array){
        return regex_array.some(r => r.test(tag));
    }

    let creator_button;
    let update_button;
    let update_clone_button;
    const update_clone_func = function(){
        console.log("[update_clone_func]");
        //反映ボタンとクリエイター記事申請ボタンの処理
        //クリエイターズ記事のタグが存在する場合には、通常の公開ボタンを押せなくする
        //逆にない場合は申請をできなくする
        update_button = null;
        Array.from(side_div.querySelectorAll("button.btn.btn-orange.btn-cta > i"),element=>{
            if(element.innerText === "反映"){
                update_button = element.parentNode;
            }
        });

        //反映ボタンの複製
        update_clone_button = update_button.cloneNode(true);
        update_clone_button.firstElementChild.innerText = "反映(改)";
        update_clone_button.addEventListener("click",()=>{
            if(window.confirm('反映してもよろしいですか？')){
                update_button.click();
            }
        });
        update_button.style.display = "none";
        update_button.parentNode.insertBefore(update_clone_button, update_button);

        check_creator_shinsei();
    }

    //クリエイターズ記事の申請中かどうか調べて、それに合わせてボタンの状態を変更
    //申請後に状態が変わるので
    const check_creator_shinsei=()=>{
        console.log("[申請中チェック]");
        if(!is_edit){
            //編集中じゃなければこのチェックは要らない
            return;
        }
        if(is_creator_shinseichu){
            //申請中から申請前に戻ることはないので、処理を打ち切る
            return;
        }

        creator_button = null;
        Array.from(side_div.querySelectorAll("button.btn.btn-blue.btn-cta"),element=>{
            if(element.firstElementChild.innerHTML==="クリエイターズ記事申請"){
                creator_button = element;
            }
        });
        //反映ボタンの複製
        let old_flag = is_creator_shinseichu;
        is_creator_shinseichu = false;
        Array.from(side_div.querySelectorAll("p.lead"),element=>{
            if(element.innerHTML.indexOf('審査中のため現在公開できません')>-1){
                is_creator_shinseichu = true;
            }
        });
        if(!is_creator_shinseichu){
            //平常時
            update_clone_button.style.display = "block";
            update_button.style.display = "none";
        }else{
            //クリエイターズ申請中
            update_clone_button.style.display = "none";
            update_button.style.display = "block";
        }
        tag_func();
    };

    const tagpage_load_wait = (e)=>{
        setTimeout(()=>{
            tg_load_func();
            tag_autocomp_func();
        },4000);
        window.open(e.target.h, "tagpage_load");
        e.stopPropagation();
    };

    //タグ入力補完支援
    //任意のタグを追加する
    const add_tag_func = (v)=>{
        if(is_tag_complite){return;};
        let hit = false;
        Array.from(tag_div.children,element=>{
            const tn = element.firstElementChild.innerHTML;
            if(tn == v){
                hit = true;
            }
        });
        if(hit){return;}

        is_tag_complite = true;
        let inp_text = v;
        inp_text = inp_text.replace(/&amp;/g, '&');
        tag_input.value = inp_text;
        const ev = new Event('input');
        tag_input.dispatchEvent(ev);

        setTimeout(()=>{
            //入力補完に出てるタグを自動クリック
            Array.from(autocomp_div.querySelectorAll("li.multiselect__element > span"),element=>{
                let tn = element.firstElementChild.innerHTML;
                if(tn.toLowerCase() == v.toLowerCase()){
                    const ev = new Event('click');
                    element.dispatchEvent(ev);
                }
            });
            is_tag_complite = false;
        },200);
    }

    //部分一致で候補一覧を作成
    const suggest_func = (tags,chks,inp,hits)=>{
        for(let i=0; i<tags.length; i++){
            if(indexOf2(chks[i],inp)>-1){
                const v = tags[i];
                if(hits.indexOf(v)==-1){
                    hits.push(v);
                }
            }
        }
    };

    //先頭一致検索（略語の自動検出用）
    const first_suggest_func = (tags,chks,inp,hits)=>{
        var s0 = inp[0];
        var s1 = inp[1];
        for(let i=0; i<tags.length; i++){
            if((chks[i][0] == s0)&&(chks[i][1] == s1)){
                const v = tags[i];
                if(hits.indexOf(v)==-1){
                    hits.push(v);
                }
            }
        }
    };

    //タグ入力補完用
    let tag_autocomp_timer = -1;
    const tag_autocomp_func = ()=>{
        //動作を安定させるために、少し待ってから実行。待ってる間に呼び出されると待ち時間リセット
        if(0<=tag_autocomp_timer){
            clearTimeout(tag_autocomp_timer);
        }
        tag_autocomp_timer = setTimeout(()=>{
            console.log("[tag_autocomp_func]");
            //検索タグの入力補完時の色分け
            let selectFlag = false;
            Array.from(autocomp_div.querySelectorAll("li.multiselect__element > span"),element=>{
                let spn;
                if(element.childElementCount > 1){
                    spn = element.childNodes[1];
                }else{
                    spn = document.createElement('a');
                    spn.style.color = "#aaaaaa";
                    spn.style.padding = "0px 0px 0px 20px";
                    spn.addEventListener('click', tagpage_load_wait, false);
                    element.appendChild(spn);
                }

                let tn = element.firstElementChild.innerHTML;
                const result = getTagColor(tn);
                element.style.background = result.background;
                element.style.color = result.color;

                if(result.visible){

                    if(hashTagCounter[tn]){
                        spn.textContent = "(" + hashTagCounter[tn] + "件)";
                    }else{
                        spn.textContent = "検索する";
                    }
                    spn.h = "https://ch.dlsite.com/search/contents?content=matomes&tag="+tn;

                    element.style.display ="block";
                    element.style["pointer-events"] = "auto";

                    if(selectFlag){
                        const ev = new Event('mouseenter');
                        element.dispatchEvent(ev);
                        selectFlag = false;
                    }
                }else{
                    //非表示タグ
                    if(is_ng_chk){
                        element.style.display ="none";
                    }
                    element.style["pointer-events"] = "none";
                    if(element.className == "multiselect__option multiselect__option--highlight"){
                        //無効タグが先頭にある場合、Enterでそのまま入力しちゃうので抑止
                        //無効タグしか無い場合は防げないけど
                        selectFlag = true;
                    }
                }
            });

            //独自の入力補完
            if (!is_tag_complite){
                const inp = normalize_tag(tag_input.value);
                tagspan_div.textContent = "";
                tagspan_div.style.color = "#aaaaaa";
                if(inp != ""){
                    let hits = [];
                    suggest_func(all_tags2,all_tags2_chk,inp,hits);
                    suggest_func(all_tags,all_tags_chk,inp,hits);
                    //先頭2文字一致
                    if(inp.length>=2){
                        first_suggest_func(all_tags2,all_tags2_chk,inp,hits);
                        first_suggest_func(all_tags,all_tags_chk,inp,hits);
                    }
                    //特殊条件一致
                    //if(f in suggest_tags){
                    //    suggest_tags[inp[0]].forEach((v)=>{hits.push(v);});
                    //}
                    Object.keys(suggest_tags2).forEach((key)=>{if(indexOf2(key,inp)>-1){hits.push(suggest_tags2[key])}});

                    //アニメタグ専用処理
                    if(inp.match(/^20\d{2}/)){
                        const h = inp.slice(0,4)+"年";
                        ["冬","春","夏","秋"].forEach((v)=>{
                            hits.push(h+v+"アニメ");
                        });
                    }

                    if(hits.length>0){
                        tagspan_div.textContent = "登録済みタグ: "
                        hits.forEach((v)=>{
                            let b = document.createElement('span');
                            b.innerHTML = v + "　";

                            b.addEventListener('click', (e)=>{
                                //一度押したら完了するまでは押せなくしておく
                                if(!is_tag_complite){
                                    add_tag_func(v);
                                }
                            }, {once:true});

                            tagspan_div.appendChild(b);
                        });
                    }
                }else{
                }
            }

            tag_autocomp_timer = -1;
        },200);
    }

    //タグの色分けと反映ボタンへの対応
    const tag_func = function(){
        let creator_flag = false;
        let draft_flag = false;
        //検索タグの文字を調べて色分け表示をする
        Array.from(tag_div.children,element=>{
            const result = getTagColor(element.firstElementChild.innerHTML);
            element.style.background = result.background;
            element.style.color = result.color;
            creator_flag = creator_flag || result.creator;
            draft_flag = draft_flag || result.draft;
        });

        //タグの状態とクリエイターズ記事申請ボタン＆反映ボタンを連動させる
        if(is_matome){
            if(creator_button){
                creator_button.disabled = ((!creator_flag)||draft_flag);
            } else {
                //クリエイター記事申請ボタンが見つからない場合、関連フラグを無視する
                //公開済みの記事に対応するため
                creator_flag = false;
            }
            //クリエイター記事申請中は公開ボタンをいじらない
            if(is_edit&&(!is_creator_shinseichu)){
                update_clone_button.disabled = (creator_flag||draft_flag);
            }
        }
    }

    //削除ボタン対応
    const delete_func = function(){
        //元のボタンを複製して新しいボタンを作成。元のボタンは非表示にしておく
        //削除ボタンが存在し、新型削除ボタンが存在しない時に生成
        if(!is_edit){return;}
        Array.from(item_div.querySelectorAll("div.btn-unit"),element=>{
            (function (){
                //ループ内で関数を作ると上手く動かないので、即時関数でくるむ
                let deleteButton, cloneButton;
                Array.from(element.getElementsByTagName("button"),btn =>{
                    if(btn.firstElementChild.innerText==="削除"){
                        deleteButton = btn;
                    }else if(btn.firstElementChild.innerText==="削除(改)"){
                        cloneButton = btn;
                    }
                });

                if(deleteButton&&(!cloneButton)){
                    cloneButton = deleteButton.cloneNode(true);
                    cloneButton.firstElementChild.innerHTML = "削除(改)";
                    cloneButton.style.background = "#FFCCCC";
                    cloneButton.addEventListener("click",()=>{
                        if(window.confirm('本当に削除しますか？')){
                            deleteButton.click();
                        }
                    });
                    deleteButton.style.display = "none";
                    deleteButton.parentNode.insertBefore(cloneButton, deleteButton);

                }
            })();
        });
    }

    //キャンセルボタン対応
    const cancel_func = ()=>{
        if(!is_edit){return;}
        console.log("cancel_func");
        //console.log(element);
        let cancelButton, cloneCancelButton;
        Array.from(item_div.querySelectorAll("button.btn.btn-gray.btn-cta-s > i"),btn=>{
            if(btn.innerText==="キャンセル"){
                cancelButton = btn.parentNode;
            }else if(btn.innerText==="キャンセル(改)"){
                cloneCancelButton = btn.parentNode;
            }
        });
        //キャンセル改がすでに存在した場合、いったん削除
        if(cloneCancelButton){
            cloneCancelButton.remove();
        }
        //リンク先を検索する時点ではボタンは不要。リンク先取得後にキャンセルボタンを置き換えないとヘンになる
        let is_link_before = false;
        Array.from(item_div.querySelectorAll("div.form-label.is-mandatory"),e=>{
            if(e.innerText === "リンクのURL"){
                is_link_before = true;
            }
        });

        if(!is_link_before){
            if(cancelButton){
                cloneCancelButton = cancelButton.cloneNode(true);
                cloneCancelButton.firstElementChild.innerHTML = "キャンセル(改)";
                cloneCancelButton.addEventListener("click",()=>{
                    if(window.confirm('編集内容が消えてしまいますが、よろしいですか？')){
                        cancelButton.click();
                    }
                });
                cancelButton.style.display = "none";
                cloneCancelButton.style.display = "";
                cancelButton.parentNode.insertBefore(cloneCancelButton, cancelButton);
            }
        }else{
            cancelButton.style.display = "";
        }
    };

    //タグが追加されたり削除されたりした時に時に呼び出す関数
    const tagedit_mo = new MutationObserver(records=>{
        tagedit_mo.disconnect();
        console.log("[tagedit_mo]");
        tag_func();
        tagedit_mo_start();
    });
    const tagedit_mo_start = ()=> tagedit_mo.observe(tag_div, {childList: true});

    //タグ編集関連のところで問題が起きた時に呼び出す関数
    const autocomp_mo = new MutationObserver((records)=>{
        autocomp_mo.disconnect();
        console.log("[autocomp_mo]");
        tag_autocomp_func();
        autocomp_mo_start();
    });
    const autocomp_mo_start = ()=> autocomp_mo.observe(autocomp_div, {characterData:true,subtree: true,});

    //アイテムの追加時。キャンセルボタンへの対応
    const itemedit_mo = new MutationObserver(records=>{
        console.log("[itemedit_mo]");
        Array.from(records,record=>{
            if(record.target.id==="items-container"){
                if(record.addedNodes.length==1){
                    //新規アイテムの作成
                    itemedit_mo.disconnect();
                    console.log("[アイテム追加]");
                    cancel_func();
                    itemedit_mo_start();
                }else if(record.removedNodes.length==1){
                    //作成をキャンセル
                    itemedit_mo.disconnect();
                    console.log("[作成をキャンセル]");
                    delete_func();
                    itemedit_mo_start();
                }
            }else if(record.target.className==="form-group"){
                if((record.previousSibling!==null)&&(record.previousSibling.className==="element-editable")){
                    //新規作成の確定、もしくは編集をキャンセル
                    itemedit_mo.disconnect();
                    console.log("[編集を確定]");
                    delete_func();
                    itemedit_mo_start();
                }else if((record.nextSibling!==null)&&(record.nextSibling.className==="element-editable")){
                    //既存アイテムの編集
                    itemedit_mo.disconnect();
                    console.log("[アイテム編集]");
                    cancel_func();
                    itemedit_mo_start();
                }
            }else if((record.previousSibling!==null)&&(record.previousSibling.className==="form-group-inner")&&(record.nextSibling===null)){
                //リンク先の読み込み
                "リンクの説明"
                console.log("[リンク先の読み込み完了]");
                itemedit_mo.disconnect();
                cancel_func();
                itemedit_mo_start();
            }
        });
    });
    const itemedit_mo_start = () => itemedit_mo.observe(item_div, {childList: true,subtree: true,});

    //下書きボタンを押した時に、クリエイターズ申請ボタンの状態が戻ってしまうので
    //bodyタグ直下にあるダイアログの出現を監視して対応する
    //あとクリエイターズ申請後の再チェックも
    const sitagaki_mo = new MutationObserver(records=>{
        console.log("[sitagaki_mo]");
        console.log(records);
        check_creator_shinsei();
    });
    const sitagaki_mo_start = () => sitagaki_mo.observe(document.body, {childList: true,attributes: true,attributeOldValue: true,});

    //タグ利用状況設定ロード
    const tg_load_func = () =>{
        const ltg = (GM_getValue("tag_counter") || "")
        const tg = ltg.split("\n");
        for(let i=0; i<tg.length; i++){
            let t= tg[i].split(",");
            if((tg[0]!=undefined)&&(tg[0]!="")){
                hashTagCounter[t[0]] = t[1];
            }
        }
    }
    const tg_save_func = () =>{
        let sv = "";
        for (const key in hashTagCounter){
            sv += key + "," + hashTagCounter[key] + "\n"
        }
        GM_setValue("tag_counter",sv);
    }

    //スクリプト設定ロード(オフライン設定系)
    const es_load_func_campaign = () =>{
        //キャンペーンのみロードする場合があるので分離
        let ct = GM_getValue("campaign_tags") || "";
        campaign_tags2 = ct.split("\n");

        if(is_edit){
            document.getElementById("es_campaign_tags").value = ct;
            let time_stamp = GM_getValue("lastupdate_campaign") || "--/--";
            let btn = document.getElementById("es_cre_load");
            btn.value = `お題タグの再読み込み\n前回の更新(${time_stamp})`;
        }
    };

    const es_load_func1 = () =>{
        //ロード処理
        let ut = GM_getValue("user_tags") || "";
        user_tags = ut.replace(/&/g, '&amp;').split("\n");
        if(is_edit){
            document.getElementById("es_user_tags").value = ut;
        }
        es_load_func_campaign();

        all_tags2 = [];
        Array.prototype.push.apply(all_tags2,user_tags);
        Array.prototype.push.apply(all_tags2,campaign_tags2);
        all_tags2_chk = normalize_tags(all_tags2);
    };

    //NG表示のチェックを切り替えた時
    const es_ng_chk_load = () =>{
        let tmp = GM_getValue("ng_chk") || "true";
        console.log("CHK_LOAD "+tmp);
        is_ng_chk = (tmp == "true");
        if(is_edit){
            document.getElementById("es_ng_chk").checked = is_ng_chk;
        }
    }

    const es_ng_chk_save = () =>{
        is_ng_chk = document.getElementById("es_ng_chk").checked;
        console.log("CHK_SAVE "+is_ng_chk)
        if(is_ng_chk){
            GM_setValue("ng_chk","true");
        }else{
            GM_setValue("ng_chk","false");
        }
    }

    //クリエイターズ記事のお題更新ボタン押した時用
    const es_cre_load_func = () =>{
        es_load_func_campaign();

        all_tags2 = [];
        Array.prototype.push.apply(all_tags2,user_tags);
        Array.prototype.push.apply(all_tags2,campaign_tags2);
        all_tags2_chk = normalize_tags(all_tags2);
    }

    //スクリプト設定ロード（ネット通信系）
    const es_load_func2 = () =>{
        let tmp = GM_getValue("fetish_tags") || "";
        if(tmp != ""){ fetish_tags = tmp.split("\n"); }
        tmp = GM_getValue("default_tags") || "";
        if(tmp != ""){ default_tags = tmp.split("\n"); }
        tmp = GM_getValue("ng_tags") || "";
        if(tmp != ""){ ng_tags = tmp.split("\n"); }

        tmp = GM_getValue("suggest_tags_key") || "";
        if(tmp != ""){
            let key = tmp.split("\n");
            tmp = GM_getValue("suggest_tags_val") || "";
            let val = tmp.split("\n");
            if(key.length != val.length){
                window.alert("入力補完用のタグの数が違います");
            }else{
                suggest_tags = {};
                for(let k in key){
                    suggest_tags[key[k]] = val[k];
                }
            }
        }

        suggest_tags2 = {};
        Object.keys(suggest_tags).forEach(key=>{
            suggest_tags2[normalize_tag(key)] = suggest_tags[key];
        });

        all_tags = [];
        Array.prototype.push.apply(all_tags,draft_tags);
        Array.prototype.push.apply(all_tags,creator_tags);
        Array.prototype.push.apply(all_tags,fetish_tags);
        Array.prototype.push.apply(all_tags,campaign_old_tags);
        Array.prototype.push.apply(all_tags,default_tags);
        all_tags_chk = normalize_tags(all_tags);

        let ajax_btn = document.getElementById("es_ajax");
        if(is_edit){
            let time_stamp = GM_getValue("lastupdate_ajax") || "--/--";
            ajax_btn.value = `最新のタグをダウンロード\n前回の更新(${time_stamp})`;
        }
    };

    //スクリプト設定ロード
    const es_load_func = () =>{
        es_load_func1();
        es_load_func2();
        es_ng_chk_load();
    }

    //ネット通信用ボタン
    const es_ajax_func = () =>{
        //GoogleAppsScriptで作ったデータにアクセスして、最新のタグ情報を取得
        let url = "https://script.google.com/macros/s/AKfycby21GNlPnP4nH224FgLVaTAreTyoOj2RXmfFRA7OggqWBm4mYa8/exec";
        fetch(url,{mode: 'cors'}).then(response => {
            if(response.ok) { // ステータスがokならば
                return response.text(); // レスポンスをテキストとして変換する
            } else {
                throw new Error(response.statusText);
            }
        }).then(text => {
            //各行に必要なタグを入れるようにする
            let tagarr = text.split("\n");

            if(tagarr[0]!="DLCE"){
                //識別フラグがない時は、なんらかのメッセージと判断してそのまま出す
                window.alert(text);
            }else if(tagarr[1].parseint>100){
                //バージョンアップ通知
                window.alert("新しい形式のデータです\nDLchannel_Extenderを更新してください");
            }else{
                let tmp = tagarr[2].split(",");
                GM_setValue("fetish_tags",tmp.join("\n"));
                tmp = tagarr[3].split(",");
                GM_setValue("default_tags",tmp.join("\n"));
                tmp = tagarr[4].split(",");
                GM_setValue("ng_tags",tmp.join("\n"));
                tmp = tagarr[5].split(",");
                GM_setValue("suggest_tags_key",tmp.join("\n"));
                tmp = tagarr[6].split(",");
                GM_setValue("suggest_tags_val",tmp.join("\n"));
                //更新時刻を保存
                GM_setValue("lastupdate_ajax",get_timestamp());

                es_load_func2();
                tag_func();

                window.alert(`読み込み成功!\n・ジャンルタグ ${fetish_tags.length}件\n・通常タグ ${default_tags.length}件\n・NGワード ${ng_tags.length}件\n・入力補完設定 ${Object.keys(suggest_tags).length}件`);
            }
        }).catch((error) => {
            window.alert(`通信エラーが発生しました\n${error}`);
        });
    }

    //スクリプト設定セーブボタン
    const es_save_func = () =>{
        GM_setValue("campaign_tags",document.getElementById("es_campaign_tags").value);
        GM_setValue("user_tags",document.getElementById("es_user_tags").value);
        es_load_func1();
        tag_func();
    }

    //開閉ボタン
    const es_open_func = () =>{
        let es = document.getElementById("extender_setting");
        if(es.style.display=="none"){
            es.style.display = "";
        }else{
            es.style.display = "none";
        }
    }
    //このスクリプトの設定画面を埋め込む
    const setting_func = () =>{
        document.querySelector(".horizontal-container > section > div > .form-container").insertAdjacentHTML("beforeend",`
<div id="extender_setting" class="form-group" style="display:none;">
    <div class="form-body" style="margin : 10px;">
        <div class="form-body-inner" style="width:250px;">
            <div class="form-label" style="background-color:#AAFFFF;">クリエイターズ企画タグ</div>
            <div>
                <textarea id="es_campaign_tags" style="resize: vertical;height: 150px;width:100%;" readonly="readonly"></textarea><br /><br />
                公式ページ左上のボタンで取得。<br />その後に再読み込みで更新<br />
                <input type="button" id="es_cre_link" style="width:100%" value="クリエイターズ\n公式ページへ" /><br />
                <input type="button" id="es_cre_load" style="width:100%" value="お題タグの再読み込み\n最終更新(--/--)" />
            </div>
        </div>
    </div>
    <div class="form-body" style="margin : 10px;">
        <div class="form-body-inner" style="width:250px;">
            <div class="form-label" style="background-color:#AAFFFF;">ユーザー設定タグ</div>
            <div>
                <textarea id="es_user_tags" style="resize: vertical;height: 250px;width:100%"></textarea><br />
                保存の時点で設定が反映されます。<br/>
                <input type="button" id="es_save" style="width:100%" value="編集したデータを保存" />
            </div>
        </div>
     </div>
    <div class="form-body" style="margin : 10px;">
        <div class="form-body-inner">
            <div class="form-label" style="background-color:#AAFFFF;">ダウンロード機能</div>
            <input type="button" id="es_ajax" style="width:100%" value="ダウンロード\n　\n　" /><br />
            <input type="button" id="es_ajax_link" style="width:100%" value="タグの一覧表を\nブラウザで開く" /><br />
            <div class="form-label" style="background-color:#AAFFFF;">その他の設定</div>
            <input type="checkbox" name="es_ng_chk" id="es_ng_chk" value="e" /><span>NGワードを含んだタグを非表示にする</span>
        </div>
    </div>
</div>`);
        let ele = document.querySelector("div.form-group:nth-child(3) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1)");
        let div = document.createElement('div');
        ele.parentNode.insertBefore(div, ele.nextSibling);

        div.insertAdjacentHTML("beforeend",'<input type="button" id="es_open" value="Extender設定画面">');
        document.getElementById("es_open").addEventListener("click", es_open_func);
        div.insertAdjacentHTML("beforeend",' <input type="button" id="es_sita" value="下書き">');
        document.getElementById("es_sita").addEventListener("click", (e)=>{add_tag_func("下書き")});
        if(creator_button){
            div.insertAdjacentHTML("beforeend",' <input type="button" id="es_cre" value="クリエイターズ記事">');
            div.insertAdjacentHTML("beforeend",'<input type="button" id="es_crenex" value="ねくすと！">');
            div.insertAdjacentHTML("beforeend",'<input type="button" id="es_osu" value="オススメ作品">');
            document.getElementById("es_cre").addEventListener("click", (e)=>{add_tag_func("クリエイターズ記事")});
            document.getElementById("es_crenex").addEventListener("click", (e)=>{add_tag_func("クリエイターズねくすと")});
            document.getElementById("es_osu").addEventListener("click", (e)=>{add_tag_func("オススメ作品レビュー")});
        }

        es_load_func();

        document.getElementById("es_save").addEventListener("click", es_save_func);
        document.getElementById("es_ajax").addEventListener("click", es_ajax_func);
        document.getElementById("es_cre_load").addEventListener("click", es_load_func_campaign);

        document.getElementById("es_ng_chk").addEventListener("change", es_ng_chk_save);

        document.getElementById("es_cre_link").addEventListener("click", ()=>{
            window.open("https://ch.dlsite.com/lp/creators" ,"_blank");
        });
        document.getElementById("es_ajax_link").addEventListener("click", ()=>{
            window.open("https://docs.google.com/spreadsheets/d/1yCUpEgc6yQFkeNA3j86hj0fTnV5wpDKAXRNXhP_16-g/edit#gid=1763499063" ,"_blank");
        });
    }

    //トーク補助表示用
    const talkedit_func = () =>{
        //console.log("[talkedit_func]");
        document.getElementById("extender_talkedit_count").innerHTML = "文字数("+document.getElementById("comment-body").value.length.toString()+"/1000)";
    }

    //入力補完用
    const tag_input_init = () =>{
        tag_div = document.querySelector("#tag-selector > div > div > div.multiselect__tags-wrap");
        autocomp_div = document.querySelector("#tag-selector > div > div.multiselect__content-wrapper");

        tag_input = document.getElementsByClassName("multiselect__input")[0];
        tag_input.addEventListener("keydown",(e)=>{
            //enterで確定しようとした時に、変なタグだった場合はそれを防ぐ
            if(e.key === 'Enter'){
                console.log("enter");
                const element = autocomp_div.querySelectorAll("li.multiselect__element > span.multiselect__option--highlight");
                if(element.length == 0){
                    console.log("zero");
                    e.preventDefault();
                }else{
                    const tn = element[0].firstElementChild.innerHTML;
                    if(!getTagColor(tn).visible){
                        console.log("NG "+tn);
                        e.preventDefault();
                    }
                }
            }
        },true);
        tagspan_div = document.createElement('div');
        tag_input.parentNode.insertBefore(tagspan_div, tag_input.nextSibling);
    }

    const start_time = new Date();
    let startup_timer;
    if(/tag=/.test(location.href)){
        startup_timer = setInterval(()=>{
            const kekka = document.querySelector("p.page-count");
            if(kekka){
                clearInterval(startup_timer);
                console.log("タグ検索画面");
                const found = /tag=([^\?&]+)/.exec(location.href);
                const tn = decodeURI(found[1]);

                const found2 = /(\d+)件中/.exec(kekka.innerHTML);
                tg_load_func();
                hashTagCounter[tn] = found2[1];
                tg_save_func();
                console.log(hashTagCounter);
            }
        },1000);
    } else if(/mypage/.test(location.href)){
        //マイページ
        //まとめ作成ボタンを付ける
        startup_timer = setInterval(()=>{
            console.log("[mypage_timer]");
            if(document.querySelector(".main-section")){
                clearInterval(startup_timer);
                console.log("[mypage]");
                let main_sec = document.querySelector(".main-section");
                main_sec.insertAdjacentHTML("beforebegin",'<div align="right"><a href="/matome/create" class="btn btn-blue btn-action-create-matome"><i>まとめを作成する</i></a></div>');
            }
        },1000);
    }else if(/lp\/creators/.test(location.href)){
        //クリエイターズのページ（企画タグを拾う用）
        document.body.insertAdjacentHTML("afterbegin",'<div><input type="button" id="es_tag_update" style="width: 400px; height: 100px" value="DLchannel_Extender用\n企画タグの読み込み" /></div>');
        document.getElementById("es_tag_update").addEventListener("click", ()=>{
            //ボタンをクリックすると、記事の中からタグ情報を抽出する
            const temp_tags = [];
            const a_elements = document.querySelectorAll("a");
            for (let a of a_elements) {
                let t = /tag=([^\?]+)/.exec(a.getAttribute('href'));
                if(t!==null){
                    temp_tags.push(decodeURI(t[1]));
                }
            }

            //プレイ動画企画終了のためいったん処理をやめる
            //if(/プレイ動画/.test(document.body.innerHTML)){
            //    temp_tags.push("プレイ動画");
            //}

            const join_tags = temp_tags.join("\n");
            GM_setValue("campaign_tags",join_tags);
            GM_setValue("lastupdate_campaign",get_timestamp());
            window.alert("タグの更新が完了しました。\n\n"+join_tags);
        });
    } else if(/matome/.test(location.href)){
        console.log("[matome]");
        //まとめ記事の場合
        //ページの読み込みが終わるまでタイマーを回して待つ
        startup_timer = setInterval(()=>{
            console.log("[startup_timer]");
            //preload-containerが非表示になっているかで、読み込み中かどうか判断
            //preload-containerはページ内に2つあって、判断基準になるのは2つ目の方
            //2つ以上の時もあるっぽい？
            let preload_containers = document.querySelectorAll("div.preload-container");
            if((preload_containers.length>=2)&&(preload_containers[1].style.display === "none")){
                //途中でエラーが発生した時に無限ループになるので、最初に止める
                clearInterval(startup_timer);
                //重要なhtmlタグは変数に記録しておいて、何度も呼び出さずに済むように
                item_div = document.getElementById("items-container");
                side_div = document.getElementsByClassName("side-container")[0];
                tag_input_init();

                //低解像度時に左側のボックスがずれるバグ対策
                document.getElementsByClassName("main-container")[0].insertAdjacentHTML("beforeend",'<div style="height:300px"></div>');
                //その他最初に動かしておくべき関数
                if(/create/.test(location.href)||/edit/.test(location.href)){
                    console.log("[is_edit]");
                    is_edit = true
                    //更新ボタンも編集時だけ
                    update_clone_func();
                    //編集画面の場合のみ、設定画面を埋め込むようにする
                    setting_func();
                    itemedit_mo_start();
                }else{
                    es_load_func();
                }
                tg_load_func();
                tag_func();
                delete_func();
                //オブザーバーの起動
                tagedit_mo_start();
                autocomp_mo_start();
                sitagaki_mo_start();
            }
        },1000);
    }else if(/talk/.test(location.href)){
        //トークの場合
        is_matome = false;
        startup_timer = setInterval(()=>{
            if(document.querySelector("#comment-body") && document.querySelector("#tag-selector")){
                //タイマーを止める
                clearInterval(startup_timer);
                //初期動作
                console.log("[startup_timer]");
                //console.log(GM_getValue("talkedit"));
                //タグ補完用
                tag_input_init();
                es_load_func();
                tg_load_func();
                tag_func();
                tagedit_mo_start();
                autocomp_mo_start();
                //ヒント領域の生成
                document.getElementById("comment-body").insertAdjacentHTML("afterend",'<span id="extender_talkedit_count">ヒント領域</span><button id="extender_talkedit_load" type="button"><i>前回の投稿をクリップボードにコピー</i></button><span id="extender_talkedit_time"></span>');
                //テキストエリアに書き込むと情報更新
                talkedit_func();
                document.getElementById("comment-body").addEventListener("keydown",talkedit_func);
                document.getElementById("comment-body").addEventListener("keyup",talkedit_func);
                document.getElementById("comment-body").addEventListener("keypress",talkedit_func);
                document.getElementById("comment-body").addEventListener("change",talkedit_func);
                //右クリックペースト時。実際にペーストが終わるまで少し待つ
                document.getElementById("comment-body").addEventListener("paste", ()=>setTimeout(talkedit_func,100));
                //書き込みボタンの複製
                let orig = document.querySelector("button.btn-blue:nth-child(2)");
                orig.insertAdjacentHTML('afterend','<button class="btn btn-blue btn-cta" id="extender_talk_submit"><i>書き込む（改）</i></button>');
                document.querySelector("#extender_talk_submit").addEventListener("click",()=>{
                    GM_setValue("talkedit",document.getElementById("comment-body").value);
                    let dt = new Date();
                    GM_setValue("talktime",dt.getFullYear()+"/"+(dt.getMonth()+1)+"/"+dt.getDate()+" "+dt.getHours()+":"+dt.getMinutes());
                    GM_setValue("talktime2",Math.floor((dt-start_time)/60000));
                    orig.click();
                    return false;
                });
                //投稿復元ボタン
                document.getElementById("extender_talkedit_load").addEventListener("click",()=>{
                    document.body.insertAdjacentHTML("beforeend",'<textarea id="extender_copy" style="position:fixed;left:-100%;">'+GM_getValue("talkedit")+'</textarea>');
                    document.getElementById('extender_copy').select();
                    document.execCommand('copy');
                    document.getElementById('extender_copy').remove();
                    window.alert("クリップボードにコピーしました");
                    document.getElementById("extender_talkedit_time").innerHTML = "前回の投稿時間("+GM_getValue("talktime")+") かかった時間(約"+GM_getValue("talktime2")+"分)";
                    return false;
                });
                orig.style.display = "none";
            }
        },1000);
    }
}());